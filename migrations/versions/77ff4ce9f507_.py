"""empty message

Revision ID: 77ff4ce9f507
Revises: 0770841893f0
Create Date: 2022-01-25 09:27:58.797162

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '77ff4ce9f507'
down_revision = '0770841893f0'
branch_labels = None
depends_on = None


def upgrade():
    op.rename_table('absent_days', 'absent_day')


def downgrade():
    op.rename_table('absent_day', 'absent_days')

