"""empty message

Revision ID: 0cc8c37a4786
Revises: 87db986d6944
Create Date: 2024-06-25 16:11:07.291733

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '0cc8c37a4786'
down_revision = '87db986d6944'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column('user', 'class_name', new_column_name='location')


def downgrade():
    op.alter_column('user', 'location', new_column_name='class_name')
