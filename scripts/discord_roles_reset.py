from lms42.routes.discord import manage_discord_user
from lms42.models.user import User
import time

for user in User.query.filter_by(is_active=True).order_by(User.short_name):
    if user.discord_id:
        print(user.short_name)
        manage_discord_user(user)
        time.sleep(30)
