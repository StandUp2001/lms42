from ..app import db
import sqlalchemy as sa
import datetime


class Event(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    title = sa.Column(sa.String, nullable=False)
    image = sa.Column(sa.String)
    description = sa.Column(sa.String, nullable=False)
    address = sa.Column(sa.String, nullable=False)
    event_time = sa.Column(sa.DateTime, nullable=False)
    registration_deadline = sa.Column(sa.DateTime, nullable=False)
    spots = sa.Column(sa.Integer, nullable=False)
    cost = sa.Column(sa.Integer, nullable=False)
    city_code = sa.Column(sa.String, nullable=False) # e or d
    registrations = db.relationship('EventRegistration', back_populates='event', cascade='all, delete-orphan')

    @property
    def image_file(self):
        if self.image:
            return f"/static/event-images/{self.image}"
        return None

    @property
    def available_spots(self):
        return self.spots - len(self.registrations)
    
    def is_user_registered(self, user):
        return EventRegistration.query.filter_by(student_id=user.id, event_id=self.id).first()
    

class EventRegistration(db.Model):
    event_id = sa.Column(sa.Integer, sa.ForeignKey('event.id', ondelete='CASCADE'), nullable=False, primary_key=True)
    event = sa.orm.relationship("Event", back_populates='registrations')

    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False, primary_key=True)
    student = sa.orm.relationship('User')

    registration_time = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.utcnow)
    bux = sa.Column(sa.Integer, nullable=False)


class BuxPayment(db.Model):
    id = sa.Column(sa.Integer, primary_key=True)
    provider_reference = sa.Column(sa.String, nullable=True)
    student_id = sa.Column(sa.Integer, sa.ForeignKey('user.id'), nullable=False)
    transaction_date = sa.Column(sa.Date, nullable=False, default=datetime.date.today)
    bux = sa.Column(sa.Integer, nullable=False)
    status = sa.Column(sa.String, nullable=False, default='pending')
