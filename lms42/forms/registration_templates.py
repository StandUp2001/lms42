import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import datetime
from ..forms.common import SeparatorField

NONE = '1000'
EXPIRED = '1001'
ACTIVE = '1002'


def parse_type(word):
    if word == "None":
        return None
    if word == "True":
        return True
    if word == "False":
        return False
    if word.isdigit():
        return int(word)
    return word


def get_registration_template_form(group):
    # The form for users to fill out to register
    class RegistrationTemplateForm(flask_wtf.FlaskForm):
        sep1 = SeparatorField('Your info')

        first_name = wtf.StringField('First name', validators=[
            wtv.DataRequired()
        ])
        last_name = wtf.StringField('Last name', validators=[
            wtv.DataRequired()
        ])
        email = wtf.StringField('(Non-Saxion) email', validators=[
            wtv.Email(), wtv.DataRequired()
        ])
        english = wtf.SelectField('Preferred language', coerce=parse_type, choices=[('None', ''), ('True', 'English'), ('False', 'Dutch')], validators=[wtv.InputRequired()])
        
        def validate_english(self, english):
            if english.data is None:
                raise wtv.ValidationError('This field is required.')
        
        student_number = wtf.IntegerField('Student number', validators=[
            wtv.Optional(strip_whitespace=True), wtv.DataRequired()
        ])

        if group.name.startswith('e'):
            extra_bux = wtf.BooleanField("I'm signing up for Tuks (€15 per year) and I'm therefore claiming 2 additional intro Bux as a bonus.")
        else:
            extra_bux = wtf.BooleanField(render_kw={'disabled':''},)
        sep2 = SeparatorField('Registration Info')

        if group.study_coach is not None:
            study_coach = wtf.StringField('Study coach', default=group.study_coach.full_name, validators=[wtv.ReadOnly()])
        group_id = wtf.StringField('Home group', default=group.name, validators=[wtv.ReadOnly()])
        submit = wtf.SubmitField('Register')
    return RegistrationTemplateForm()


class CreateRegistrationTemplateForm(flask_wtf.FlaskForm):
    # form for teachers to create a template
    group_id = wtf.SelectField('Group', choices=[])
    date = wtf.DateField('Expire date', default=datetime.date.today() + datetime.timedelta(days=7))
    submit = wtf.SubmitField('Save')
