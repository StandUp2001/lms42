import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf


# Separator field
class SeparatorField(wtf.HiddenField):
    pass
