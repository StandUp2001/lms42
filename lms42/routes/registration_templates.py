from ..app import db, app, get_base_url, schedule_job
from ..models.user import User, LoginLink
from ..models.registration_templates import RegistrationTemplate
from ..models.group import Group
from ..models.event import BuxPayment
from ..utils import role_required
import flask
import datetime
import random

import sqlalchemy as sa
from ..forms.registration_templates import (
    EXPIRED,
    ACTIVE,
    get_registration_template_form,
    CreateRegistrationTemplateForm,
)
SECRET_CHARACTERS = ["c", "d", "e", "f", "h", "j", "k", "m", "n", "p", "r", "t", "v", "w", "x", "y", "2", "3", "4", "5", "6", "8", "9"]


def generate_secret():
    return ''.join(random.choice(SECRET_CHARACTERS) for _ in range(7))


def delete_expired_templates():
    # Expire tokens if they're more than 7 days old
    RegistrationTemplate.query.filter(
        RegistrationTemplate.date < datetime.datetime.utcnow()
    ).delete()
    db.session.commit()


# Every day (mon-sun) in the middle of the night delete all expired templates.
schedule_job(
    delete_expired_templates,
    trigger="cron",
    day_of_week="mon-sun",
    hour=0,
    minute=0,
    second=0,
)


def get_group_choices():
    # Gets all the current groups in the database
    return [(group.id, group.name) for group in Group.query.order_by(Group.name).all()]


def filter_registration_templates(templates, args):
    # Filters the template on the giving args (group_id)
    if group_id := args.get("group_id"):
        templates = templates.filter_by(group_id=int(group_id))

    if expired := args.get("expired"):
        if expired == EXPIRED:
            templates = templates.filter(
                RegistrationTemplate.date < datetime.date.today()
            )
        elif expired == ACTIVE:
            templates = templates.filter(
                RegistrationTemplate.date >= datetime.date.today()
            )

    return templates


def get_short_name(name, last_name):
    # Gets a unique short_name based on first and last name

    name = name.strip().lower()
    if not User.query.filter_by(short_name=name).all():
        return name

    suffix = "".join(word[0] for word in last_name.split()).lower()
    if not User.query.filter_by(short_name=name+suffix).all():
        return name+suffix

    num = 2
    while True:
        new_name = name + str(num)
        if not User.query.filter_by(short_name=new_name).all():
            return new_name
        num += 1


# Show all templates that fall in line with the filter
@app.route("/registration_templates", methods=["GET", "POST"])
@role_required("teacher")
def show_templates():
    registration_templates = RegistrationTemplate.query.order_by(
        RegistrationTemplate.date
    )
    return flask.render_template(
        "registration-templates.html",
        registration_templates=registration_templates.all(),
        base_url=get_base_url(),
        title="Registration templates",
    )


# create a template
@app.route("/registration_templates/create", methods=["GET", "POST"])
@role_required("teacher")
def create_template():
    form = CreateRegistrationTemplateForm()
    form.group_id.choices += get_group_choices()

    if form.validate_on_submit():
        registration_template = RegistrationTemplate()
        form.populate_obj(registration_template)
        registration_template.secret = generate_secret()
        db.session.add(registration_template)
        db.session.commit()
        return flask.redirect("/registration_templates")

    return flask.render_template(
        "generic-form.html",
        form=form,
        action="/registration_templates/create",
        title="Create a registration template",
    )


# edit a template
@app.route(
    "/registration_templates/<registration_template_id>/edit", methods=["GET", "POST"]
)
@role_required("teacher")
def edit_template(registration_template_id):
    template = RegistrationTemplate.query.get(registration_template_id)
    form = CreateRegistrationTemplateForm(obj=template)
    form.group_id.choices += get_group_choices()

    if form.validate_on_submit():
        form.populate_obj(template)
        db.session.commit()
        return flask.redirect("/registration_templates")

    return flask.render_template(
        "generic-form.html",
        form=form,
        action=f"/registration_templates/{registration_template_id}/edit",
        title="Create a registration template",
    )


# Delete a template
@app.route("/registration_templates/<registration_template_id>/delete", methods=["GET"])
@role_required("teacher")
def delete_template(registration_template_id):
    registration_templates = RegistrationTemplate.query.get(registration_template_id)
    db.session.delete(registration_templates)
    db.session.commit()

    flask.flash(f"Registration template {registration_templates.id} deleted.")
    return flask.redirect("/registration_templates")


# Register form based on template
@app.route("/register/<registration_template_secret>", methods=["GET", "POST"])
def register_template(registration_template_secret):
    # Check if template is valid
    template = RegistrationTemplate.query.filter_by(
        secret=registration_template_secret
    ).first()
    if not template:
        flask.flash("Invalid template")
        return flask.redirect("/")

    # Get the form based on template
    form = get_registration_template_form(template.group)
    if form.validate_on_submit():
        try:
            user = User()
            form.populate_obj(user)
            user.group_id = template.group_id

            user.short_name = get_short_name(form.first_name.data, form.last_name.data)
            user.intro_bux = 12 if form.extra_bux.data else 10

            db.session.add(user)
            db.session.commit()

            transaction = BuxPayment(
                student_id=user.id,
                bux=user.intro_bux,
                provider_reference="signup:"+template.group.name,
                status="paid",
            )
            db.session.add(transaction)
            db.session.commit()


            flask.flash(f"Hi {user.first_name}, welcome to Software Development!")
            link = LoginLink()
            link.user = user
            link.redirect_url = "/people/me"
            return link.login()

        except sa.exc.IntegrityError as e:
            db.session.rollback()
            if "uq_user_student_number" in str(e):
                form.student_number.errors.append("Student number already in use.")
            elif "uq_user_email" in str(e):
                form.email.errors.append("Email address already in use.")
            elif "uq_user_short_name" in str(e):
                form.first_name.errors.append(
                    "Something went wrong creating short name!"
                )

    return flask.render_template(
        "generic-form.html",
        intro="This will create your account on our Learning Management System, and add you to your home group in one go.",
        form=form,
        action=f"/register/{registration_template_secret}",
        title="Register",
    )
