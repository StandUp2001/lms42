from ..app import app
import flask
from .. import working_days


@app.route('/', methods=['GET'])
def frontpage():
    return flask.render_template('frontpage.html')


@app.route('/coc', methods=['GET'])
def coc():
    current_year, _octant = working_days.get_current_octant()
    # SHow only this academic year and the next one
    week_starts = {year: weeks for year, weeks in working_days.WEEK_STARTS.items() if year in {current_year, current_year+1}}

    version = flask.request.args.get('version')
    return flask.render_template(f'coc{"-"+version if version else ""}.html', week_starts=week_starts, special_weeks=working_days.SPECIAL_WEEKS)


@app.route('/happy-coding', methods=['GET'])
def happy_coding():
    return flask.render_template('happy-coding.html')


@app.route('/python-cheat-sheet', methods=['GET'])
def python_cheat_sheet():
    with open('lms42/templates/python-cheat-sheet.md') as file:
        markdown = file.read()
    return flask.render_template('markdown.html', title="Python cheat sheet", body=markdown)


@app.route('/humans.txt', methods=['GET'])
def humans_txt():
    with open('AUTHORS') as file:
        authors = file.read()
    return flask.Response(authors, mimetype='text/plain')
