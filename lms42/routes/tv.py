from ..app import app
from ..models.user import get_all_people
import flask


@app.route('/tv', methods=['GET'])
@app.route('/tv/<device>', methods=['GET'])
def tv(device=None):
    fragment: str = flask.request.args.get('fragment', '')
    template = fragment if fragment == 'queue' else 'tv'

    group_filter = None
    if device == 'd':
        group_filter = "d%"
    elif device in {'e', 'a', 'b'}:
        group_filter = "e%"
    elif device == 'iglo':
        group_filter = "e%"

    students = get_all_people(group_filter=group_filter) if group_filter else None
    
    return flask.render_template(template+'.html',
        header=False,
        device=device,
        students=students
    )
