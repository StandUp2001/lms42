from ..app import db, app, schedule_job, retry_commit
from ..assignment import Assignment
from ..models.attempt import Attempt, Grading
from ..models import curriculum
from ..utils import role_required
from flask_login import current_user
from werkzeug.utils import secure_filename
import flask
import random
import os
import sqlalchemy as sa
import datetime


@app.route('/inbox', methods=['GET'])
@role_required('teacher')
def get_inbox():
    return flask.render_template('inbox.html',
        students=get_inbox_students(),
        group_by="teacher_short_name",
    )


def get_inbox_students():
    
    query = sa.sql.text("""
select
    student.id,
    student.short_name,
    t.short_name as teacher_short_name,
    (case when student.avatar_name is null then '/static/placeholder.png' else '/static/avatars/' || student.avatar_name end) as avatar,
    array_remove(array_agg(tag || ':' || color || ':' || coalesce(url,'') order by prio desc, prio_time asc), null) as tags,
    (time_log.end_time > (localtime at time zone 'Europe/Amsterdam')::time - interval '5 minutes') as online
from (
    select
        student_id,
        'approval' tag,
        'important' as color,
        10 as prio,
        attempt.start_time as prio_time,
        null as teacher_id,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where attempt.status='awaiting_approval'

    union

    select
        id,
        'check-in' tag,
        'info' as color,
        8 prio,
        last_check_in_time as prio_time,
        check_in_teacher_id as teacher_id,
        null as url
    from "user"
    where check_in_teacher_id is not null

    union

    select
        student_id,
        (case when attempt.status='needs_consent' then '👀 '||node_id else node_id end) as tag,
        (case when credits>0 then 'warning' else 'default' end) as color,
        (case when attempt.status='needs_consent' then 2 when attempt.credits>0 then 1 else 0 end) as prio,
        attempt.submit_time as prio_time,
        attempt.grade_teacher_id as teacher_id,
        '/curriculum/' || attempt.node_id || '?student=' || student_id || '#attempt_' || number as url
    from attempt
    where attempt.status='needs_grading' or attempt.status='needs_consent'

    union

    select
        student_id,
        'exam deadline' tag,
        '*important' color,
        21 prio,
        attempt.deadline_time as prio_time,
        null as teacher_id,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and now() > attempt.deadline_time

    union

    select
        student_id,
        'no recording' tag,
        '*warning' color,
        20 prio,
        attempt.deadline_time prio_time,
        null as teacher_id,
        null as url
    from attempt
    where attempt.status='in_progress' and attempt.credits>0 and extract(epoch from (now() - attempt.last_screenshot_time)) > 65
 
) as tags
join "user" as student on tags.student_id = student.id
left join "user" as t on tags.teacher_id = t.id
left join time_log on time_log.user_id=student.id and date=current_date
where student.is_active=true
group by t.id, student.id, time_log.user_id, time_log.date
order by t.short_name nulls first, max(prio) desc, min(prio_time) asc
;
""")

    cutoff = datetime.datetime.now().replace(hour=10, minute=30)
    return db.session.execute(query, {
        "cutoff_time": cutoff,
    }).fetchall()


@app.route('/inbox/grade/<attempt_id>', methods=['POST'])
@role_required('teacher')
def grading_post(attempt_id):
    form = flask.request.form

    attempt = Attempt.query.get(attempt_id)
    # If the teacher has clicked not graded this sets the attempt to ungraded and returns to the student panel
    if 'not-graded' in form:
        attempt.set_ungraded()
        return flask.redirect(f"/people/{attempt.student_id}#attempts")

    if attempt.status in {"passed", "repair", "failed", "needs_consent", "fraudulent"}:
        flask.flash("Attempt was already graded! Your grading has replaced the old grading, but the old grading is still available in the database.")
    elif attempt.status != "needs_grading":
        flask.flash(f"Attempt is still in {attempt.status}!?")
        return flask.redirect("/inbox")
    
    for file in flask.request.files.getlist("grading_upload"):
        os.makedirs(os.path.join(attempt.directory, "grading"), exist_ok=True)
        file.save(os.path.join(attempt.directory, "grading", secure_filename(file.filename)))

    ao = Assignment.load_from_directory(attempt.directory)

    scores = ao.form_to_scores(form)
    motivations = ao.form_to_motivations(form)

    grade, passed = ao.calculate_grade(scores)

    # All exams with 5, 6 and 10 grades need consent, as well as those with >= 15 ECTS and a random 10% of the rest.
    needs_consent = "ects" in ao.node and (grade in {5, 6, 10} or ao.node["ects"] >= 15 or random.randrange(0, 10) == 0 or bool(form.get('request_consent')))
    
    if 'mark_fraudulent' in form:
        needs_consent = False
        passed = False
    
    grading = Grading(
        attempt_id=attempt.id,
        grader_id=current_user.id,
        objective_scores=scores,
        objective_motivations=motivations,
        grade=grade,
        grade_motivation=form.get('motivation'),
        passed=passed,
        needs_consent=needs_consent,
    )

    if needs_consent:
        attempt.status = "needs_consent"
    elif "ects" in ao.node and 'mark_fraudulent' in form:
        attempt.status = "fraudulent"
    elif "ects" in ao.node:
        attempt.status = "passed" if passed else "failed"
    elif form.get('formative_action') in {"failed", "passed", "repair"}:
        attempt.status = form.get('formative_action')
    else:
        attempt.status = "passed" if passed else "repair"

    db.session.add(grading)
    db.session.commit()

    attempt.write_json()

    student = attempt.student
    if attempt.status == "passed":
        flask.flash(f"{student.full_name} passed with a {round(grade)}!")
    elif attempt.status == "needs_consent":
        flask.flash("Grading needs consent from another teacher.")
    elif attempt.status == "fraudulent":
        flask.flash("Submit a report to the examination board regarding this potential exam fraud")
    # elif grade is None:
    #     flask.flash(f"{student.full_name} has not received a grade.")
    else:
        flask.flash(f"{student.full_name} did NOT pass with a {round(grade)}.")
    if "ects" in ao.node and attempt.status != "needs_consent" and attempt.status != "fraudulent":
        grading.announce()
    
    return flask.redirect(f"/people/{attempt.student_id}#attempts")


@app.route('/inbox/<int:attempt_id>/grade_teacher', methods=['PUT'])
@role_required('teacher')
def set_grade_teacher(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    teacher_id = flask.request.form.get('grade_teacher')
    attempt.grade_teacher_id = teacher_id or None
    db.session.commit()
    return "OK"


@retry_commit
def assign_exams_for_grading():
    node_ids = [node_id for node_id, node in curriculum.get('nodes_by_id').items() if node.get("grade_without_student")]
    attempts: list[Attempt] = Attempt.query \
        .filter(Attempt.node_id.in_(node_ids)) \
        .filter(Attempt.status.in_(['needs_grading', 'needs_consent'])) \
        .filter_by(grade_teacher_id=None)

    for attempt in attempts:
        attempt.assign_grade_teacher()


schedule_job(func=assign_exams_for_grading, trigger="cron", day_of_week='mon-fri', hour=8, minute=0, second=0)