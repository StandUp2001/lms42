from ..app import db, app, retry_commit
import flask
from flask_login import current_user
import datetime
import sqlalchemy

CHECK_IN_DEADLINE = datetime.time(10, 30)

TIME_LOG_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time)
values(:user_id, :date, :time, :time)
on conflict (user_id, date)
do update set end_time = :time
returning start_time
""")

CHECK_IN_ASSIGN_QUERY = sqlalchemy.text("""
update "user"
set check_in_teacher_id = :teacher_id
where id=:student_id
""")

GRADER_ASSIGN_QUERY = sqlalchemy.text("""
update attempt
set grade_teacher_id = :teacher_id
where (status = 'needs_grading' or status = 'needs_consent') and grade_teacher_id is null and student_id = :student_id
""")
                           

SUSPICIOUS_QUERY = sqlalchemy.text("""
insert into time_log(user_id, date, start_time, end_time, suspicious)
values(:user_id, :date, '7:00', '7:00', true)
on conflict (user_id, date)
do update set suspicious = true
""")


@app.route('/time_log/ping', methods=['GET'])
@retry_commit
def add_time_log():
    now = datetime.datetime.now().replace(microsecond=0)
    ip = flask.request.headers.get('X-Forwarded-For', flask.request.remote_addr)

    if not current_user or not current_user.is_authenticated:
        return {"error": "Not logged in"}
    if not ip.startswith(("145.76.", "145.2.",)) and not ip.startswith(("::ffff:145.76.", "::ffff:145.2.",)) and ip != "127.0.0.1" and ip != "::1":
        return {"error": f"Not a Saxion IP ({flask.request.remote_addr})"}
    if now.hour < 7 or now.hour >= 22:
        db.session.execute(SUSPICIOUS_QUERY, {"user_id": current_user.id, "date": now.date()})
        print(f"Time log outside time window: user_id={current_user.id} now={now}")
        return {"error": "Outside 7:00-22:00 time window"}
    time = now.time()
    start_time = db.session.execute(TIME_LOG_QUERY, {"user_id": current_user.id, "date": now.date(), "time": time}).fetchone()[0]
    if start_time == time:
        if time <= CHECK_IN_DEADLINE:
            # The person is first showing up today, and it's before the check-in deadline.
            teacher_id = current_user.todays_group_teacher_id
            if teacher_id:
                db.session.execute(CHECK_IN_ASSIGN_QUERY, {"student_id": current_user.id, "teacher_id": teacher_id})
                db.session.execute(GRADER_ASSIGN_QUERY, {"student_id": current_user.id, "teacher_id": teacher_id})
        else:
            flask.flash(f"Note: as you seem to have arrived after {CHECK_IN_DEADLINE.strftime('%-I:%M')}, you have not been scheduled for feedback/check-in.")

    db.session.commit()
    return {"success": True}
