const commentTextArea = document.querySelector('textarea#comments')
if (commentTextArea) {
    const storageKey = 'nodeFeedback-' + location.pathname.split('/').pop()
    // Load comments from localStorage || null
    let feedback = localStorage.getItem(storageKey);
    
    // Set previously added comment.
    if ( feedback != null ) {
        commentTextArea.value = feedback;
    } 
    
    // Store the value in the localStorage on input.
    commentTextArea.addEventListener('input', function() {
        localStorage.setItem(storageKey, commentTextArea.value);
    });
    
    // If the assignment is submitted, the localStorage can be cleared.
    const submitButton = document.querySelector('#submit[name=submit]')
    submitButton.addEventListener('click', function() {
        localStorage.removeItem(storageKey);
    });
}