'use strict';

(function() {
    let expandList = document.getElementsByClassName('details-expand');
    let blockList = document.getElementsByClassName('details-block');
    let params = new URLSearchParams(window.location.search)
    const lesson = params.get('lesson')
    if (!lesson) return
    
    for(let num=0; num<blockList.length; num++) {
        let nodes = blockList[num].querySelectorAll(`.node-${lesson}`)
        if (nodes.length > 0){
            blockList[num].style.display = '';
            expandList[num].classList.add('shown')
            for (let i = 0; i < nodes.length; i++) {
                nodes[i].classList.add('highlight')
            }
        }
    }
})();
