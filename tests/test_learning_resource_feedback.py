from lms42.models.learning_resource import LearningResource

from common import run_attempt


def feedback_on_resource(client, resource_id, feedback):
    """
    Sends a POST request to provide feedback on a learning resource.

    Args:
        client (FlaskClient): The Flask test client used to send the request.
        resource_id (int): The ID of the learning resource to provide feedback on.
        feedback (str): The type of feedback to provide ('like' or 'dislike').

    Returns:
        Response: The response object returned by the Flask test client. (learning-resource-feedback-buttons.html)
    """
    return client.post(
        "/feedback/learning-resource",
        data={
            "resource_id": resource_id,
            "feedback_type": feedback,
        },
        no_html=True,
    )


def test_resources_being_added_to_db(client):
    client.login("student1")
    run_attempt(client, "linux", start=True, submit=False, grade=None)

    learning_resources = LearningResource.query.all()

    response = feedback_on_resource(client, learning_resources[0].id, "like")
    assert response.status_code == 200
    assert f'id="feedback-{learning_resources[0].id}"' in response.get_data(
        as_text=True
    )
    assert 'class="feedback-count" title="Likes: 1"' in response.get_data(as_text=True)
    assert 'class="like-button clicked"' in response.get_data(as_text=True)

    response = feedback_on_resource(client, learning_resources[0].id, "dislike")
    assert response.status_code == 200
    assert f'id="feedback-{learning_resources[0].id}"' in response.get_data(
        as_text=True
    )
    assert 'class="feedback-count" title="Likes: 0"' in response.get_data(as_text=True)
    assert 'class="dislike-button clicked"' in response.get_data(as_text=True)


def test_multiple_feedback_types(client):
    # Log in as student1 and run an attempt
    client.login("student1")
    run_attempt(client, "linux", start=True, submit=False, grade=None)

    learning_resources = LearningResource.query.all()
    resource_id = learning_resources[0].id

    # Like the resource
    response = feedback_on_resource(client, resource_id, "like")
    assert response.status_code == 200, "Failed to submit 'like' feedback"
    assert 'class="feedback-count" title="Likes: 1"' in response.get_data(as_text=True)

    # Dislike the resource
    response = feedback_on_resource(client, resource_id, "dislike")
    assert response.status_code == 200, "Failed to submit 'dislike' feedback"
    assert 'class="feedback-count" title="Likes: 0"' in response.get_data(as_text=True)
    assert 'class="feedback-count" title="Dislikes: 1"' in response.get_data(
        as_text=True
    )


def test_feedback_removal(client):
    client.login("student1")
    run_attempt(client, "linux", start=True, submit=False, grade=None)

    learning_resources = LearningResource.query.all()
    resource_id = learning_resources[0].id

    # Like the resource
    response = feedback_on_resource(client, resource_id, "like")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 1"' in response.get_data(as_text=True)

    # Like the resource again to remove the like
    response = feedback_on_resource(client, resource_id, "like")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 0"' in response.get_data(as_text=True)


def test_liking_and_disliking_resource_multiple_users(client):
    client.login("student1")
    run_attempt(client, "linux", start=True, submit=False, grade=None)

    learning_resources = LearningResource.query.all()
    resource_id = learning_resources[0].id

    # Student 1 likes the resource
    response = feedback_on_resource(client, resource_id, "like")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 1"' in response.get_data(as_text=True)

    # Student 2 logs in and likes the resource
    client.login("student2")
    response = feedback_on_resource(client, resource_id, "like")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 2"' in response.get_data(as_text=True)

    # Student 2 dislikes the resource
    response = feedback_on_resource(client, resource_id, "dislike")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 1"' in response.get_data(as_text=True)
    assert 'class="feedback-count" title="Dislikes: 1"' in response.get_data(
        as_text=True
    )

    # Student 1 dislikes the resource
    client.login("student1")
    response = feedback_on_resource(client, resource_id, "dislike")
    assert response.status_code == 200
    assert 'class="feedback-count" title="Likes: 0"' in response.get_data(as_text=True)
    assert 'class="feedback-count" title="Dislikes: 2"' in response.get_data(
        as_text=True
    )


def test_find_resource_on_assignment_page(client):
    # Logging in as student1
    client.login("student1")

    # Running an attempt on the assignment
    run_attempt(client, "linux", start=True, submit=False, grade=None)

    # Getting the first learning resource ID
    learning_resources = LearningResource.query.all()

    # Accessing the curriculum page for Linux
    response = client.get("/curriculum/linux")

    # Finding the learning resources element by its class, and matching it with the number of learning resources in the database
    resource_element = client.find(".learning-resource-card").require(
        len(learning_resources)
    )

    # Asserting that the response status is 200 OK
    assert response.status_code == 200
    # Asserting that the learning resource element is found
    assert resource_element is not None
