Feature: total number of calculations

    Rule: you can see the total number of calculations

        Example: Bob does 3 calculations
            Given Bob uses the maths app
            When Bob adds the 1 + 5 calculation
            And Bob adds the 4 - 3 calculation
            And Bob adds the 5 * 2 calculation
            Then the counter shows 3 calculations

    Rule: you can see the number of correct and incorrect answers

        Background:
            Given Bob uses the maths app
            And Bob navigated to the Quiz section
            And  Bob started a quiz

        Example: Bob gives a wrong answer
            When Bob answers 0 questions correctly and 1 questions incorrectly
            Then Bob can see 0 correct answers
            And Bob can see 1 incorrect answers

        Example: Bob gives a correct answer
            When Bob answers 1 questions correctly and 0 questions incorrectly
            Then Bob can see 1 correct answers
            And Bob can see 0 incorrect answers

