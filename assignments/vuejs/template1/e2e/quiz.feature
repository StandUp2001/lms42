Feature: quiz

    Background:
        Given Bob uses the maths app
        And Bob navigated to the Quiz section
        And  Bob started a quiz

    Rule: You can not go to the next question without providing a numeric answer

        Example: Bob does not provide an answer
            When Bob answers nothing
            Then Bob cannot submit the answer
            And Bob does not see another question

        Example: Bob answers 4
            When Bob answers 4
            Then Bob can submit the answer
            And Bob sees another question

    Rule: After ten  questions, you can see your result

        Example: Bob gets 4 answers right
            When Bob answers 4 questions correctly and 6 questions incorrectly
            Then Bob sees that the quiz has been completed
            And Bob sees that he has a score of 4 out of 10

        Example: Bob does not finish
            When Bob answers 2 questions correctly and 3 questions incorrectly
            Then Bob sees another question