
Feature: adding and removing calculations

    Background: Bob uses the app
        Given Bob uses the maths app

    Rule: When you add a calculation, the result is computed

        Scenario Outline: Bob does calculation
            When Bob adds the <calc> calculation
            Then Bob sees the result <res>

            Examples:
                | calc  | res |
                | 1 + 2 | 3   |
                | 6 / 2 | 3   |
                | 2 * 3 | 6   |
                | 5 - 4 | 1   |

    Rule: You can remove a calculation

        Example: Bob removes an addition
            Given Bob has added the 1 + 5 calculation
            When Bob deletes the calculation
            Then the calculation is gone

    Rule: You can add multiple calculations

        Example: Bob adds two calculations
            When Bob adds the 1 + 5 calculation
            And Bob adds the 4 - 3 calculation
            Then there are 2 calculations