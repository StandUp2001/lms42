Feature: view information about the maths app

    Rule: you can read about the application

        Example: Bob reads about the app
            Given Bob uses the maths app
            And Bob navigated to the About section
            Then Bob can read about the maths app