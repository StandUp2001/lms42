Feature: sum questions

    Background:
        Given Bob uses the maths app
        And Bob navigated to the Sums section
        And Bob added a sum

    Rule: you can generate sum questions
        Example: Bob generates a sum question
            Then Bob sees a sum question

    Rule: you can remove sum questions
        Example: Bob removes a sum question
            When Bob deletes a sum question
            Then Bob sees no sum questions

    Rule: if you provide an answer, the answer is marked
        Example: Bob provides the wrong answer
            When Bob provides the wrong answer
            Then Bob receives feedback that his answer is wrong

        Example: Bob provides the correct answer
            When Bob provides the correct answer
            Then Bob receives feedback that his answer is correct