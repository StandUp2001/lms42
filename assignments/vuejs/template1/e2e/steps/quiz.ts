import { createBdd } from 'playwright-bdd';
import { expect } from 'playwright/test';

const { Given, When, Then } = createBdd();

async function currentQuestion(page) {
    return page.getByText('Question').textContent();
}

async function giveAnswer(page, question, isCorrect) {
    const current = await currentQuestion(page);
    question.current = current;
    const operants = /(\d+)\s*\+\s*(\d+)/;
    const match = current.match(operants);
    const sum =
        parseInt(match[1], 10) + parseInt(match[2]) + (isCorrect ? 0 : 1);
    await page.getByPlaceholder('Your answer').fill(sum.toString());
}

Given('Bob started a quiz', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

Then('Bob cannot submit the answer', async ({ page, $test }) => {
    // todo
    // the button should be disabled, when you cannot submit
    $test.fixme();
});

Then('Bob does not see another question', async ({ page, question, $test }) => {
    const previousQuestion = question.current;
    // todo
    // current should be the same as previous
    $test.fixme();
});

When('Bob answers nothing', async ({ page, question }, answer: string) => {
    question.current = await currentQuestion(page);
    // do nothing
});

When('Bob answers {int}', async ({ page, question, $test }, answer: number) => {
    question.current = await currentQuestion(page);
    // todo
    $test.fixme();
});

Then('Bob can submit the answer', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

Then('Bob sees another question', async ({ page, question, $test }) => {
    const previousQuestion = question.current;
    // todo
    // current should not be the same as previous
    $test.fixme();
});

When(
    'Bob answers {int} questions correctly and {int} questions incorrectly',
    async ({ page, question, $test }, correct: number, incorrect: number) => {
        // todo
        $test.fixme();
    }
);

Then('Bob sees that the quiz has been completed', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

Then(
    'Bob sees that he has a score of {int} out of {int}',
    async ({ page, $test }, correct: number, total: number) => {
        // todo
        $test.fixme();
    }
);

Then(
    'Bob can see {int} correct answers',
    async ({ page, $test }, correct: number) => {
        // todo
        $test.fixme();
    }
);

Then(
    'Bob can see {int} incorrect answers',
    async ({ page, $test }, incorrect: number) => {
        // todo
        $test.fixme();
    }
);
