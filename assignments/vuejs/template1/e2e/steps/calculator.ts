import { createBdd } from 'playwright-bdd';
import { expect } from '@playwright/test';

const { Given, When, Then } = createBdd();

Given('Bob uses the maths app', async ({ page }) => {
    await page.goto('/');
});

Given('Bob navigated to the {word} section', async ({ page }, section) => {
    await page.getByRole('button', { name: section }).click();
});

Then('Bob can read about the maths app', async ({ page }) => {
    await expect(page.getByText('Lorem ipsum dolor sit amet,')).toBeVisible();
});

const addCalculation = When(
    'Bob adds the {int} {word} {int} calculation',
    async ({ page }, firstOperant, operator, secondOperant) => {
        await page.getByRole('button', { name: 'Add Calculation' }).click();
        const emptyCalc = page.getByLabel('calc-item').last();
        await emptyCalc
            .getByPlaceholder('Value A')
            .fill(firstOperant.toString());
        await emptyCalc
            .getByLabel('operator')
            .filter({ hasText: undefined })
            .selectOption(operator);
        await emptyCalc
            .getByPlaceholder('Value B')
            .fill(secondOperant.toString());
    }
);

Given(
    'Bob has added the {int} {word} {int} calculation',
    async ({ page }, firstOperant, operator, secondOperant) => {
        await addCalculation({ page }, firstOperant, operator, secondOperant);
    }
);

Then('Bob sees the result {int}', async ({ page }, result) => {
    await expect(page.getByLabel('result')).toHaveText(result.toString());
});

When('Bob deletes the calculation', async ({ page }) => {
    await page.getByRole('button', { name: 'delete' }).click();
});

Then('the calculation is gone', async ({ page }) => {
    await expect(page.getByLabel('calc-time')).not.toBeVisible();
});

Then('there are {int} calculations', async ({ page }, numberOfCalcs) => {
    await expect(page.getByLabel('calc-item')).toHaveCount(numberOfCalcs);
});

Then('the counter shows {int} calculations', async ({ page }, total) => {
    await expect(page.getByText('No# Calculations:')).toContainText(
        total.toString()
    );
});
