import { test as base } from 'playwright-bdd';

type Question = {
    valueA: number;
    valueB: number;
    operator: '+' | '-' | '*' | '/';
};

type MyFixtures = {
    question: Question;
};

export const test = base.extend<MyFixtures>({
    question: async ({}, use) => {
        await use({ current: '' });
    },
});
