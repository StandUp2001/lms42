import { createBdd } from 'playwright-bdd';
import { expect } from 'playwright/test';

const { Given, When, Then } = createBdd();

async function getLastSumItemValues(page) {
    const lastSum = await page.getByLabel('sum-item').last();
    const { valueA, valueB } = await lastSum.evaluate(
        (sum) => sum.__vnode.ctx.props //hack to retrieve randomly generated question
    );
    return { valueA, valueB };
}

Given('Bob added a sum', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

Then('Bob sees a sum question', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

When('Bob deletes a sum question', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

Then('Bob sees no sum questions', async ({ page, $test }) => {
    // todo
    $test.fixme();
});

When(
    'Bob provides the {word} answer',
    async ({ page }, rightOrWrong: 'correct' | 'wrong') => {
        const { valueA, valueB } = await getLastSumItemValues(page);
        const answer = valueA + valueB + (rightOrWrong === 'correct' ? 0 : 1);
        await page.getByLabel('answer').fill(answer.toString());
    }
);

Then(
    'Bob receives feedback that his answer is {word}',
    async ({ page }, rightOrWrong: 'correct' | 'wrong') => {
        const sum = await page.getByLabel('sum-item');
        const feedback = rightOrWrong === 'wrong' ? 'Incorrect.' : 'Correct!';
        const color =
            rightOrWrong === 'wrong'
                ? 'rgb(255, 205, 210)'
                : 'rgb(200, 230, 201)';

        await expect(sum).toContainText(feedback);
        await expect(sum).toHaveCSS('background-color', color);
    }
);
