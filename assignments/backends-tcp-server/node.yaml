name: TCP
description: Write an HTTP/1.1 server with a TCP-based protocol.
goals:
    tcpudp: 1
    protocols: 1
days: 2
pair: true
resources:
    HTTP Specification:
        -
            link: https://http.dev/1.1
            title: HTTP/1.1
            info: Quick explanation of what HTTP is and how it works.
        -
            link: https://www.ietf.org/rfc/rfc1945.txt
            title: Hypertext Transfer Protocol -- HTTP/1.0
            info: Memo for HTTP/1.0 containing the specifications for the protocol, this was later finalized into the standard HTTP/1.1 which is still used to this day.

assignment:
    - |
        In this assignment we will implement the HTTP protocol in NodeJS to create a website that will record user data.
    - Protocol specification:
        - |
            HTTP/1.1 is a communication standard for the world-wide web.
            HTTP is built on top of the TCP protocol with plain-text (e.g. UTF-8) messages to request, and receive data from HTTP servers.

            ### Request:
            An HTTP client (e.g. your browser) sends out a request to a web server like this:
            ```
            GET /homepage HTTP/1.1
            Host: 127.0.0.1:8080
            User-Agent: nodejs-fetch/2.22.0
            Accept-Encoding: gzip, deflate
            Accept: */*
            Connection: keep-alive
            ```

            The request is formatted as follows:
            ```
            <method> <url> HTTP/1.1
            [<header field>: <value>]
            [...]
            ```

            ### Response:
            An HTTP server then send a response back to the HTTP client, a simple response could look like this:
            ```
            HTTP/1.1 200 OK
            Content-Type: text/plain; charset=utf-8

            Hello world!
            ```

            The response is formatted as follows:
            ```
            HTTP/1.1 <response code> <reason phrase>
            [<header field>: <value>]
            [...]

            [response body]
            ```

            ### Experiment:
            You can use netcat to experiment with HTTP to better understand the request and response.

            First create a file for your request called `request.txt`:
            ```
            GET / HTTP/1.1
            Host: example.com
            Connection: close


            ```
            Make sure to include the empty lines at the end as they mark the end of the request. Then pipe the file into netcat:
            ```
            nc example.com 80 < request.txt
            ```
            See if you understand what is returned back to you by the server.

    - Custom Web Server:
        -
            link: https://www.youtube.com/watch?v=_O-JheIGKko
            title: How to create a TCP Server with Nodejs
            info: This video shows how to create a TCP server wih Node.js
        -
            ^merge: feature
            text: |
                <wired-card style="width: 100%;">
                    <h1>Home</h1>
                    <p>Welcome unknown user.</p>
                </wired-card>

                <script>loadScript("/static/wired-elements.js")</script>

                Create a TCP server which shows a simple HTML page when accessed through a browser.

                You should handle a TCP connection as follows:

                - Listen for an HTTP request with a specific URL
                - Create a valid HTTP response header
                - Append a response body with the necessary HTML
                - Close the TCP socket after sending the response
                
                Send a 404 response if an invalid URL is requested.

                The wire frames in this assignment act as an example of what the expected result could look like.
                The server should generate valid HTML, but no further styling is necessary.

                Use `socket.setEncoding("utf8");` to receive TCP messages as plain text.

                <div class="notification is-important">
                    Do not use the Node.js HTTP module. In this assignment we're implementing the HTTP protocol ourselves.
                </div>
        -
            ^merge: feature
            text: |
                <wired-card style="width: 100%;">
                    <h1>Home</h1>
                    <p>Welcome unknown user. Submit your name to join the fun.</p>
                    <div>
                        <label>Name:</label>
                        <wired-input placeholder="Your name..." rows="1.5"></wired-input>
                        <wired-button>Submit</wired-button>
                    </div>
                </wired-card>

                <wired-card style="width: 100%;">
                    <h1>Home</h1>
                    <p>Welcome back Olivier.</p>
                </wired-card>

                <script>loadScript("/static/wired-elements.js")</script>

                Currently we're calling the user "unknown". We should ask for their name by including a form on our HTML page.

                Check what your server receives when a form is submitted. You can use either a form with a GET, or a POST method.
                Extract the submitted name from the request, and include it in your HTML when you have it.
        -
            ^merge: feature
            text: |
                <wired-card style="width: 100%;">
                    <h1>Home</h1>
                    <p>Welcome back Olivier. Times you've visited this website: 4</p>
                </wired-card>

                <script>loadScript("/static/wired-elements.js")</script>

                When the user reloads the page they are no longer shown their name because we haven't saved it anywhere.

                Use a cookie to store the users name, then when a user accesses the home page we can show them a more personalized page. 
                Now that we have a way to identify the user, we can also keep track of how often the user has visited our website.
                
                The page should function as follows:

                - Only show the form when the cookie isn't set.
                - Show a personal message if the cookie is set.
                - Set a cookie when the form is submitted.
                - When the user reloads the page, the personal message is still shown.
                - Each time a user with a cookie visit the page, the visit count for that user is increased.
                - The visit count for a known user is shown on te page.

                *Hint:* In a previous module you will have used cookies to identify users, investigate how cookies work within the HTTP protocol.
        -
            ^merge: feature
            text: |
                <wired-card style="width: 100%;">
                    <h1>Home</h1>
                    <p>Welcome back Olivier. Times you've visited this website: 5</p>
                    <a>View recorded user data</a>
                </wired-card>

                <wired-card style="width: 100%;">
                    <h1>User Data</h1>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>User Agent</th>
                            <th>IP</th>
                            <th>Number of visits</th>
                        </tr>
                        <tr>
                            <td>Mario</td>
                            <td>Mozzarella/5.0 (Bowser; K00-p4) Fireflower/64.0</td>
                            <td>1.2.3.64</td>
                            <td>3</td>
                        </tr>
                    </table>
                </wired-card>

                <script>loadScript("/static/wired-elements.js")</script>

                Most browsers send information about the user's system and setup to web servers. 
                Web servers can use this information to determine how, or if they should serve the user.
                For example what encryption methods are supported, or what country a user is trying to access the website from.

                Some of this information is protected by privacy laws and requires consent from the user before it can be stored.
                Once stored, it can be quite useful to us.
                For example, we can get better insights about how people are using our website, where they're from,
                and what systems they use so we can make sure they get the best possible experience.
                
                Explore what user information you can find in the HTTP requests,
                make sure to try different browsers as they may give different results.

                Then implement the following:

                - When a user submits their name, record their IP address, device type, browser and/or operating system. Whichever are available.
                - Create a second page which displays the recorded information for each user in a table.
                - On the homepage, when a user is known: Show a link to the user data page

                The user data only has to be stored in memory.

