name: Maintainer review
description: Another round of reviews this time by a maintainer, hopefully followed by a merge.
days: 2
allow_longer: false
public_for_students: true
type: project
grading: false
upload: true
grade_without_student: true
goals:
    handle_feedback: 1

intro:
- |
    <div class="notification is-warning">
        Please note that this assignment counts as part of the module exam.
    </div>

assignment:
    Entry requirement:
    -
        must: true
        title: Presentation
        text: You must do your presentation *before* submitting this assignment.

    Upload and submit: |
        Once you've finished the *Code review* and *Presentation* assignments, you may start this assignment, put all merge request URLs in `merge-requests.txt` and then directly upload and submit this assignment.

    Maintainer review: |
        After submit, there will be an additional round of code reviews, this time performed by one of the project maintainers (also known as teachers, in this case). We generally expect you to do as many rounds of improvements as needed (hopefully zero) to have your code accepted. A merge request may also be *rejected*. This can have various reasons: 
        
        - You created something the maintainers just don't want to include in LMS42. This *may* be caused by a failure to communicate and collaborate properly up front, which would have a negative impact on your *contributorship* grade. 
        - You created something that seemed like a good idea initially, but turns out to be a bad idea after all. These things happen, and don't necessarily reflect badly on your work, though it's not fun.
        
        You may also coordinate with your reviewer to retract (one of) your merge requests, if fixing does not seem viable. This will of course reflect on your grades, but may be a sensible option if you have created other significant merge requests as well or if you're going for the retake.

    Merged?:
    -
        must: true
        title: All merge requests accepted
        text: |
            The teacher will *fail* this assignment without further comments if one or more merge requests were not yet deemed ready for acceptance. If this happens, you should start this assignment again, handle the teacher's code reviews on GitLab, and when you think everything is perfect, upload and submit the assignment again.

            After this objective has been passed, the teacher will start and complete the *Grading* assignment for you.
    