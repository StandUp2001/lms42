Feature: view information about the maths app

    Rule: you can read about the application

        Example: Bob reads about the app
            Given Bob uses the maths app
            When Bob navigates to the about section
            Then Bob can read about the maths app