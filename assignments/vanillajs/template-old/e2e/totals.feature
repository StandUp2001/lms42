Feature: total number of calculations

    Rule: you can see the total number of calculations
        Example: Bob does 3 calculations
            Given Bob uses the maths app
            When Bob adds the 1 + 5 calculation
            And Bob adds the 4 - 3 calculation
            And Bob adds the 5 * 2 calculation
            Then the counter shows 3 calculations

