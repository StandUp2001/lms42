import { createBdd } from 'playwright-bdd';
import { expect } from '@playwright/test';

const { Given, When, Then } = createBdd();

Given('Bob uses the maths app', async ({ page }) => {
    await page.goto('/');
});

When('Bob navigates to the about section', async ({ page }) => {
    await page.getByRole('listitem', { name: 'About' }).click();
});

Then('Bob can read about the maths app', async ({ page }) => {
    await expect(page.getByText('Lorem ipsum dolor sit amet,')).toBeVisible();
});

const addCalculation = When(
    'Bob adds the {int} {word} {int} calculation',
    async ({ page }, firstOperant, operator, secondOperant) => {
        await page.getByRole('button', { name: 'Add Calculation' }).click();
        const emptyCalc = page.getByText('+ - * / Delete', {
            exact: true,
        });
        await emptyCalc
            .getByPlaceholder('Value A')
            .fill(firstOperant.toString());
        await emptyCalc
            .getByLabel('operator')
            .filter({ hasText: undefined })
            .selectOption(operator);
        await emptyCalc
            .getByPlaceholder('Value B')
            .fill(secondOperant.toString());
    }
);

Given(
    'Bob has added the {int} {word} {int} calculation',
    async ({ page }, firstOperant, operator, secondOperant) => {
        await addCalculation({ page }, firstOperant, operator, secondOperant);
    }
);

Then('Bob sees the result {int}', async ({ page }, result) => {
    await expect(page.getByLabel('result')).toHaveText(result.toString());
});

When('Bob deletes the calculation', async ({ page }) => {
    await page.getByLabel('delete').click();
});

Then('the calculation is gone', async ({ page }) => {
    await expect(page.getByLabel('calc-time')).not.toBeVisible();
});

Then('there are {int} calculations', async ({ page }, numberOfCalcs) => {
    await expect(page.getByLabel('calc-item')).toHaveCount(numberOfCalcs);
});

Then('the counter shows {int} calculations', async ({ page }, total) => {
    await expect(page.getByLabel('total-calculations')).toHaveText(
        total.toString()
    );
});

// New steps for saving calculations

When('Bob saves the calculations', async ({ page }) => {
    await page.getByRole('button', { name: 'Save Calculation' }).click();
});

When('Bob navigates to the saved calculations section', async ({ page }) => {
    await page.getByLabel('saved').click();  // Targets aria-label="saved"
});

Then('Bob sees the saved {int} {word} {int} calculation', async ({ page }, firstOperant, operator, secondOperant) => {
    // Ensure the saved calculations are loaded
    await page.waitForSelector('[aria-label="calc-item"]');
    
    // Get the first saved calculation item
    const calcItem = page.locator('[aria-label="calc-item"]').first();

    // Check the value of the first input
    await expect(calcItem.locator('input[placeholder="Value A"]')).toHaveValue(firstOperant.toString());

    // Check the value of the operator select
    await expect(calcItem.locator('select[aria-label="operator"]')).toHaveValue(operator);

    // Check the value of the second input
    await expect(calcItem.locator('input[placeholder="Value B"]')).toHaveValue(secondOperant.toString());
});


When('Bob deletes the saved calculation', async ({ page }) => {
    const calcItem = page.locator('[aria-label="calc-item"]').first();
    
    // Ensure the delete button is visible
    await expect(calcItem.locator('button[aria-label="delete"]')).toBeVisible();

    // Click the delete button within the first saved calculation item
    await calcItem.locator('button[aria-label="delete"]').click();
});

Then('the saved calculation is gone', async ({ page }) => {
    const calcItems = await page.locator('[aria-label="saved-calc-item"]');
    await expect(calcItems).toHaveCount(0); // Check that no saved calculations are present
});

Then('there are {int} saved calculations', async ({ page }, numberOfCalcs) => {
    await expect(page.getByLabel('saved-calc-item')).toHaveCount(numberOfCalcs);
});
