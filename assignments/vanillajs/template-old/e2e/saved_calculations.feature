Feature: Saved Calculations

  Background: Bob uses the app
    Given Bob uses the maths app

  Rule: You can save and view saved calculations

    Scenario: Bob saves a calculation and sees it in the Saved Calculations tab
      Given Bob has added the 5 + 3 calculation
      When Bob saves the calculations
      And Bob navigates to the saved calculations section
      Then Bob sees the saved 5 + 3 calculation

    Scenario: Bob deletes a saved calculation
      Given Bob has added the 4 * 2 calculation
      When Bob saves the calculations
      And Bob navigates to the saved calculations section
      And Bob deletes the saved calculation
      Then the saved calculation is gone