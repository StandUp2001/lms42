# Internship feedback

Dear internship supervisor, thank you for helping our students grow into their roles as professional software developers! We'd very much appreciate it if you could complete this Markdown form, by adding your (possibly very short) responses at the dots.

Student name: ...
Company supervisor name: ...
Company supervisor job title: ...
Company: ...
Date: ...

Please write one or more sentences in response to each of the sections in the student's `report.md` below. The listed questions are just some suggestions, so feel free to ignore any of them.

## 1. The basics
> Any inaccuracies?

...

## 2. Journal
> Does this appear to match reality (at a quick glance)?

...

## 3. Tech stack
> Any inaccuracies? Does it capture the most important parts? Does the student represent her/his own usage fairly? Anything else?

...

## 4. Contributions
> Does the student represent her/his contributions fairly? To what extend do these contributions live up to your expectations from the intern? Anything else?

...

## 5. Roles
> Any misrepresentations? Any missing roles that juniors may come in contact with? Does the student represent her/his own collaboration fairly? Anything else?

...

## 6. Development process
> Any inaccuracies? Does it capture the most important parts? Are the recommendations valuable? Anything else?

...

## Closing remarks for the intern
> Did the intern live up to your expectations? Any advise on how to improve further? Anything else?

...

## Closing remarks for the degree programme
> Based on what you've seen from our student(s) and from us, is there anything you'd like to share with us? Anything we can do to improve? 

...

## Thanks a lot!

Please email this file back to the intern as well as to the Saxion study coach. If you have any further information or other topics you'd like to discuss with us without the student in the loop, please feel free to contact the study coach directly.
