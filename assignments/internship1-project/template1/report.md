# Internship report


----------------------------------------


## 1. The basics

- Your name: 
- Internship: first or second
- Company name: 
- Company supervisor name: 
- Saxion study coach name: 
- Start date: 
- End date: 

- Company description: 
- Company head count: 
- Team description: 
- Team head count: 


----------------------------------------


## 2. Journal

...

Below are a template and an example. **TODO**: Please remove once you don't need them anymore.

- DOW YYYY-MM-DD
    - Accomplished
        - ?
        - ?
    - Learned
        - ?
        - ?
    - Next
        - ?
        - ?

- Monday 1970-01-01
    - Accomplished
        - Finished writing tests for the flux capacitor widget. Test coverage is now at about 80%, but that seems to cover all complex cases.
        - Created first design sketches for the mobile fusion reactor. This is a bit harder expected. I'll continue sketching tomorrow, but if this doesn't work out, we can always choose to go for an ordinary molten salt reactor.
    - Learned
        - The theoretical basics of nuclear fusion. This is still pretty abstract.
    - Next
        - How are fusion reactors built in practice?
        - How can incredibly hot plasma be contained without melting the container?
        - Are there any simple approached that can be follow for miniaturizing large machines?  


----------------------------------------


## 3. Tech stack

...

Below are a template and an example. **TODO**: Please remove once you don't need them anymore.

### SOME_TECH
- Description: 
- Company usage: 
- My usage: 
- Curriculum: 
- Opinion: 

### Python
- Description: A high-level interpreted programming language.
- Company usage: Example.com uses Python for its API backend.
- My usage: I created a new REST endpoint and fixed a couple of backend bugs.
- Curriculum: Python usage at Example.com uses type hints and annotations much more heavily than we learned in the curriculum.
- Opinion: I would prefer to use a statically typed language for the backend, to prevent many run-time bugs.

### Comparison

**TODO**: Second internship only. Remove this section if this is your first internship. (Though it may be smart to think ahead!)

| | COMPANY_NAME_A | COMPANY_NAME_B |
| -- | -- | -- |
| Backend language | PHP | Go |
| Backend framework | Laravel | Home grown |
| Development environment | Docker compose | Custom shell script |
| Backend tests | Extended Cypress test suite | Non-existent |
| Production hosting | Dedicated servers | AWS Lambda |
| Database | MySQL | AWS Amplify |
| Linting | Etc... | Etc... |
| Etc...

----------------------------------------


## 4. Contributions

...

Below is a template. **TODO**: Please remove once you don't need it anymore.

### EXAMPLE_FEATURE

- What: 
- Learning: 
- Duration: 
- Team work: 
- Demo: ![](media/example-feature.webp) or [video](./media/example-feature-after.webm)
- Source code: [patch](./sources/example-feature.patch) or directory `./sources/example-repo/`
- Status: 


----------------------------------------


## 5. Roles

...

Below is a template. **TODO**: Please remove once you don't need it anymore.

### SOME_ROLE_TITLE

- Description: 
- Count: 
- Profile: 
- Typical collaboration: 
- Your collaboration: 


----------------------------------------


## 6. Development process

### Description

..._

### Comparison

**TODO**: Second internship only. Remove this section if this is your first internship. (Though it may be smart to think ahead!)

| | COMPANY_NAME_A | COMPANY_NAME_B |
| -- | -- | -- |
| Sprints | 2 weeks | 4 weeks + 2 cooldown |
| Daily standups | Long discussions | Brief status updates |
| Weekly meetings | None | Plan adjustment |
| Bug fixing | Direct if urgent, or planned for sprint | During cooldown |
| Retrospective | Etc... | Etc... |
| Etc...

### Recommendations

- ...
- ...
- ...
