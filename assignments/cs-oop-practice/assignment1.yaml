Assignment: |
    In this assignment, you will create:

    1. A game library for developing text-based games on a simple canvas.
    2. Two games that utilize your library.
    
    To help you understand the objectives, let's introduce the games:

"Game 1: Rocket Game": |
    The game should resemble the following:

    <div style="max-width:608px"><div style="position:relative;padding-bottom:62.171052631579%"><iframe id="kaltura_player" type="text/javascript"  src='https://cdnapisec.kaltura.com/p/1488881/embedPlaykitJs/uiconf_id/52267962?iframeembed=true&entry_id=1_e4yrdmph&config[provider]={"widgetId":"1_x705nkfu"}&config[playback]={"startTime":0}'  allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" sandbox="allow-downloads allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation" title="RocketGame" style="position:absolute;top:0;left:0;width:100%;height:100%;border:0"></iframe></div></div>
    <!-- Location video ^ https://video.saxion.nl/media/RocketGame/1_e4yrdmph -->

    The game should have the following features:

    - The player should be able to move the rocket up and down using the arrow keys. The ship should not be allowed to move past the end of the screen.
    - Asteroids should come hurling to the left towards the rocket at three different horizontal speeds, indicated by three different colors. Each asteroid should also have a random but low vertical speed. Asteroids can be either small or large.
    - Fire bullets with the spacebar, scoring points by hitting asteroids. This can be done at most twice per second.
    - When a bullet hits an asteroids, the bullet disappears and the player's score gets incremented by one. If the asteroid was small, it disappears as well. If it was big, it splits up into two small asteroids, moving in slightly different directions.
    - Display the player's score at the top middle of the screen.
    - End the game when an asteroid collides with the rocket, displaying the total score in the middle of the screen.

"Game 2: Block Jumper": |
    The game should resemble the following:

    <div style="max-width:608px"><div style="position:relative;padding-bottom:62.171052631579%"><iframe id="kaltura_player" type="text/javascript"  src='https://cdnapisec.kaltura.com/p/1488881/embedPlaykitJs/uiconf_id/52267962?iframeembed=true&entry_id=1_2oyvoaxh&config[provider]={"widgetId":"1_852p604v"}&config[playback]={"startTime":0}'  allowfullscreen webkitallowfullscreen mozAllowFullScreen allow="autoplay *; fullscreen *; encrypted-media *" sandbox="allow-downloads allow-forms allow-same-origin allow-scripts allow-top-navigation allow-pointer-lock allow-popups allow-modals allow-orientation-lock allow-popups-to-escape-sandbox allow-presentation allow-top-navigation-by-user-activation" title="BlockJumperGame" style="position:absolute;top:0;left:0;width:100%;height:100%;border:0"></iframe></div></div>
    <!-- Location video ^ https://video.saxion.nl/media/BlockJumperGame/1_2oyvoaxh -->

    The game should have the following features:

    - Navigate the jumper in four directions using arrow keys, preventing it from moving off-screen.
    - The goal is to stay in the game for as long as possible. A score bar is shown at the top of the screen. Once the score reaches 2700 (~90 seconds), the game ends, congratulating the player.
    - Blocks of arbitrary sizes (between 1 and 15 in both dimensions) and with a random character (# @ O X or &) are coming from the top, right, bottom and left. If a block hits the jumper, the score will be halved and the jumper will change appearance for two seconds and not be hittable again during that time. Blocks moving horizontally are twice as fast as blocks moving vertically.

    - Jump with the spacebar, temporarily altering the jumper's appearance and making it invulnerable to blocks during the jump. After landing, the jumper enters *recovery mode* for two seconds, during which it has yet another appearance and cannot jump (but *can* be hit).

Game library: |
    Create a game library designed to facilitate the development of retro-style text-mode games in an easy and object-oriented manner.

    The library should...
    - Emphasize generality, avoiding any specific ties to predefined games.
    - Utilize two types of Entity objects for representing and rendering on-screen elements:
        - Rectangles with customizable width and height, filled using a single character.
        - Multi-line strings for creating complex shapes, as exemplified in the provided template games..
    - Include a game loop responsible for handling keyboard input, updating entities, and refreshing the screen at a rate of 30 times per second.
    - Internally leverage Avalonia for graphical operations without exposing Avalonia specifics to games using the library..
    - Offer collision detection mechanisms between entities based on their bounding boxes (the smallest enclosing rectangle around each shape)..
    - Prioritize efficiency without aiming for exceptional performance benchmarks..

Class diagram: |
    Here is a possible (but incomplete) high-level class diagram for the library and both games. It is provided for your convenience. You are not required to follow it to the letter, or at all.

    ```plantuml
    @startuml
    skinparam packageStyle rectangle
    skinparam linetype ortho

    top to bottom direction

    package RetroGameLib {
        class GameWindow {
            +GameWindow()
            #InitializeGame(Game): void
            #OnKeyDown(EventArgs): void
            #OnKeyUp(EventArgs): void
            -RedrawGame(): void
        }

        class Game {
            +Width: int
            +Height: int
            +Entities: List<Entity>
            +PressedKeys: List<string>
            +Score: int
            +Running: bool
            +AddEntity(entity: Entity): void
            +RemoveEntity(entity: Entity): void
            +GetCollidingEntities<T>(entity: Entity): List<T>
            +Quit(endMessage: string): void
            +{abstract} Update(): void
        }

        GameWindow -right-> "1" Game

        abstract class Entity {
            +X: double
            +Y: double
            +DisplayWidth: double
            +DisplayHeight: double
            -_visual: Avalonia.Control.TextBlock
            +{abstract} GetContent(): string
            
            #Entity(x: double, y: double, color: Color?)
            +{abstract} Update(): void
        }

        note left of Entity
            Entities should have access to a 
            Avalonia TextBox they are drawn in to
            properly calculate their Width and Height
        end note

        abstract class ShapeEntity {
            -shape: string
            +GetContent(): string
            #ShapeEntity(shape: string, x: double, y: double, color: Color?)
            #SetShape(shape: string): void
        }

        abstract class RectangleEntity {
            -width: int
            -height: int
            -character: int
            +GetContent(): string
            #ShapeEntity(character: char, x: double, y: double, width: int, height: int, color: Color?)
        }

        ShapeEntity -up-|> Entity
        RectangleEntity -up-|> Entity
        Game -down-> "0..*" Entity
    }

    package BlockJumper {
        class Program {
            #{static}Main(args: string[]): void
            +{static}BuildAvaloniaApp(): AppBuilder
        }

        class MainWindow {
            -_game: BlockJumperGame
            +MainWindow()
        }

        MainWindow --> "1" BlockJumperGame

        class BlockJumperGame {
            +BlockJumperGame(width: int, height: int)
            +Update(): void
        }

        class BlockJumper {
            +Jumper(game: Game)
            +Update(Game game): void
        }

        class Block {
            -Block(character: char, x: int, y: int, width: int, height: int, color: Color, xVelocity: double, yVelocity: double)
            +Update(game: Game): void
            +CreateRandom(game: Game): Block
        }

        class ScoreDisplay {
            +ScoreDisplay()
            +void Update(game: Game): void
        }

        Program -down-> BlockJumperGame
        Block -up-|> RectangleEntity
        ScoreDisplay -up-|> ShapeEntity
    }

    BlockJumper.BlockJumperGame -up-|> RetroGameLib.Game
    BlockJumper.BlockJumper -up-|> RetroGameLib.ShapeEntity

    @enduml
    ```

Some Help Given: |
    In the template provided, you'll find initial code to get started with. This includes:

    - Three projects (one for the library and two for the games)..
    - A structured window setup. You'll notice an OnGameTick() method in the GameWindow class, which serves as a hook for running your game loop (updating entities and rendering everything on screen).
    - Boilerplate code to run your applications (App.axaml, app.manifest, and Program.cs). You typically won't need to modify these files.
    - Within each game project, a Shapes.cs file containing constants for all shapes required to be drawn on screen.
    
    You only need to interact with Avalonia in 3 ways:
    - Utilizing a RedrawGame() method in GameWindow.axaml.cs to render all entities correctly on screen.
    - Ensuring your Entity class interacts with the TextBlock where it's rendered to accurately calculate DisplayWidth and DisplayHeight (essential for collision detection).
    - Passing down entity colors in various contexts (the only exception where Avalonia functionality may extend beyond the library project).

Objectives:
-
    title: Library functionality, convenience and flexibility
    text: |
        The library must encompass all functionalities specified above and required by both games. It should prioritize ease of use and flexibility, enabling programmers to swiftly develop similar games.

    1: 25% of features implemented in a generic and easy to use way.
    2: 50% ...
    3: 75% ...
    4: 100% ...
    map:
        libraries: 1
        abstraction: 1
        interfaces: 1
        decomposition: 1
        class-diagram: 1
        inheritance: 1
        static: 1

-
    title: Rocket Game
    ^merge: feature
    code: 0
    text: |
        The game should work as specified and shown in the video above.
    map:
        refactor: 1
        classes: 1
        class-diagram: 1
        inheritance: 1
        static: 1
-
    title: Block Jumper
    ^merge: feature
    code: 0
    text: |
        The game should work as specified and shown in the video above.
    map:
        refactor: 1
        classes: 1
        class-diagram: 1
        inheritance: 1
        static: 1
-
    title: Code quality
    ^merge: codequality
    map:
        libraries: 0.5
        abstraction: 0.5
        interfaces: 0.5
        decomposition: 0.5
        class-diagram: 0.5
        inheritance: 0.5
        static: 0.5
        refactor: 0.5
        classes: 0.5
    text: |
        Object-oriented programming (OOP) code quality guidelines:

        - Prefer using private fields over making data public through properties, unless there's a specific reason to expose them.
        - Methods intended for internal use within a class should be private. If they are needed for subclasses to override, consider marking them as protected.
        - Place functionality within methods of the class to which it primarily pertains. If functionality involves multiple classes, consider splitting it up. If it doesn't require instance data, make it a static method.
        - Minimize the creation of setters for properties to avoid exposing internal state unnecessarily. This helps keep the actual functionality within the class itself.
        - Use abstract classes and abstract methods instead of relying on duck typing. Abstract classes define a contract for subclasses, promoting clearer expectations and interfaces.
-
    title: Documentation
    map: inline-doc
    text: |
        Our library lacks documentation. Use DocFX to generate HTML documentation for the RetroGames Library. Ensure to include the following:

        - Descriptive explanations for each public class, method, and property.
        - Clearly define expected inputs and outputs where applicable.
        - Provide usage examples for complex functions.

        Document all public methods and properties comprehensively. This documentation should enable developers unfamiliar with your library to implement games without needing to refer to code or examples. Be sure to clarify the purpose and significance of method arguments and return values.

    0: The docs are useless / absent.
    2: The docs are helpful, but one would frequently need to refer to the code to study usage details.
    3: Good enough to make a quick start, without looking at the code.
    4: Documentation as one would expect for a well-known library.
    map: inline-doc