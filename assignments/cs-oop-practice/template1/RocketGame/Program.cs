﻿using Avalonia;
using Avalonia.ReactiveUI;
using RocketGame;

namespace BlockJumper;

class Program
{
    static void Main(string[] args) => BuildAvaloniaApp().StartWithClassicDesktopLifetime(args);

    public static AppBuilder BuildAvaloniaApp()
        => AppBuilder.Configure<App>()
            .UsePlatformDetect()
            .LogToTrace()
            .UseReactiveUI();
}
