﻿namespace RocketGame;

internal static class Shapes
{
    internal const string ROCKET = @"   __
   \ \_____
###[==_____>
   /_/   
";

    internal const string SMALL_ASTEROID = "/\\\n\\/";
    internal const string BIG_ASTEROID = "/\\\n<XX>\n\\/";

    internal const string BULLET = "o";
}
