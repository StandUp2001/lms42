using RetroGameLib;

namespace RocketGame;

public partial class MainWindow : GameWindow
{
    private readonly RocketGame _game;

    public MainWindow()
    {
        InitializeComponent();
        _game = new RocketGame((int)Width, (int)Height);
        InitializeGame(_game);
    }
}
