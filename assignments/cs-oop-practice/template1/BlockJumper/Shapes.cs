﻿namespace BlockJumper;

internal static class Shapes
{
    internal const string NormalShape = @" /---\
| o o |
 \---/";

    internal const string JumpShape = @" /|||\
| O O |
 \---/";

    internal const string RecoverShape = @" /---\
| - - |
 \---/";

    internal const string DeadShape = @" /---\
| x x |
 \---/";
}
