using RetroGameLib;

namespace BlockJumper
{
    public partial class MainWindow : GameWindow
    {
        private readonly BlockJumperGame _game;

        public MainWindow()
        {
            InitializeComponent();
            _game = new BlockJumperGame((int)Width, (int)Height);
            InitializeGame(_game);
        }
    }
}
