using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Threading;

namespace RetroGameLib;

public partial class GameWindow : Window
{
    private Game _game;
    private readonly DispatcherTimer _timer;

    public GameWindow()
    {
        InitializeComponent();

        // Set up the timer
        _timer = new DispatcherTimer
        {
            Interval = TimeSpan.FromMilliseconds(1000 / Game.FPS)
        };
        _timer.Tick += OnGameTick;
    }

    protected void InitializeGame(Game game)
    {
        _game = game;
        _timer.Start();
    }

    private void OnGameTick(object sender, EventArgs e)
    {
        throw new NotImplementedException();
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        base.OnKeyDown(e);
        throw new NotImplementedException();
    }

    protected override void OnKeyUp(KeyEventArgs e)
    {
        base.OnKeyUp(e);
        throw new NotImplementedException();
    }
}
