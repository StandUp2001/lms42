using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Media;
using Avalonia.Threading;
using System;

namespace TicTacToe.Fancy;

public partial class MainWindow : Window
{
    private readonly DispatcherTimer _timer;

    public MainWindow()
    {
        InitializeComponent();

        // Set up the timer
        _timer = new DispatcherTimer
        {
            Interval = TimeSpan.FromMilliseconds(1000 / 30) // Approximately 30 ticks per second
        };
        _timer.Tick += Update; //This makes sure that every tick the Update method gets called
        
        _timer.Start();
    }

    private void Update(object? sender, EventArgs e)
    {
        //Todo: Add own implementation
    }

    protected override void OnKeyDown(KeyEventArgs e)
    {
        base.OnKeyDown(e);

        //Todo: Add own implementation
    }
}
