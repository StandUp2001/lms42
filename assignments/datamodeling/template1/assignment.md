# Data modeling

## Case 1: StackOverflow

### Entities

### Logical ERD

We've made a start for you:

```plantuml
!include https://sd42.nl/static/style-light.plantuml

' Note that the asterisk (*) is used set the attribute as mandatory (NOT NULL)

entity User {
    *name
}

entity Question {
    *title
    *timestamp
    description
}

User ||--|{ Question
```

### Physical ERD

We've made a start for you:

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity User {
    *id: integer
    --
    *name: text
}

entity Question {
    *id: integer
    --
    *title: text
    *timestamp: timestamp
    *user_id: integer <<FK>>
    description: text
}

User ||--|{ Question
```


## Case 2: Twitter

### Physical ERD

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity TODO {

}

```


## Case 3: *up to you*

```plantuml
!include https://sd42.nl/static/style-light.plantuml

entity TODO {
    
}

```