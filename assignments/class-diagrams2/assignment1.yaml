- Assignment: |
    Let's build a flexible ERP (Enterprise Resource Planning) system, that allows your users to manage any type of *resources*!

    Wrapping your head around this assignment can be tricky. We recommend the following approach:
    - Study the *Features overview*. Don't worry if you fully understand it yet.
    - Look over the *Example output*. 
    - Now study the *Feature overview* again, now making sure you really understand what you need to build by referring the the *Example output*.
    - Ask someone if you don't fully understand how the application should behave.

- Features overview: |
    - Users can create new *collections* (similar to SQL tables). Some examples: `Students`, `Vehicles`, `Devices`, `Accounts`. 
    - Each *collection* has a fixed set of *attributes* (similar to SQL columns). Each attribute has a *label* text (like: `name`, `badge number`, `weight`, `price`) and an *attribute type*. 
    - The following *attribute types* are supported:
        - Text: any string.
        - Number: a float or int number. When an *attribute* of this *attribute type* is added to a *collection*, the user can set optional minimum and maximum values for this *attribute*. An attribute with the label `star rating` may have a minimum value of `1` and a maximum value of `5`. 
        - Date: a string that matches the `dd-mm-yyyy` format, like `19-01-2038`.
        - Link: a reference to an *instance* of some *collection*. The user can specify which *collection* the *attribute* links to at the time the *attribute* is added to the *collection*.
    - Each *collection* has a list of *instances*. For example, the `Teachers` collection may have an *instance* for `Frank van Viegen`.
    - Each *instance* contains *fields* for each of *attributes* of its *collection*. For example, the `Frank van Viegen` instance may have the value `Frank` for the *first name* attribute and `15000` for the *salary* attribute.
    - *Instances* may be added, deleted and modified.
    - An *attribute* may be marked as *required* or *not required*. In the later case, the *field* value the *instance* has for this *attribute* may be left blank.
    - All state is automatically saved to and loaded from disk.
    - Generate a graph image that shows the links between all instances. (Using a graph drawing library.)

- Example output: |
    When first starting the application, this is how a first *collection* can be added. Also notice the error handler.
    ```
    Starting with an empty database.

    Collections:
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> a
    Collection name (eg. 'Employees' or 'Vehicles')?
    >> teachers
    Let's add some attributes. The first attribute will be used when showing a collection list.

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 0
    Invalid selection.
    >> z
    Invalid selection.
    >> 1
    Attribute label?
    >> name
    Is this a required field?
    [y] Yes
    [n] No
    >> z
    Invalid selection.
    >> Y

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 3
    Attribute label?
    >> start date
    Is this a required field?
    [y] Yes
    [n] No
    >> n

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 2
    Attribute label?
    >> salary
    Is this a required field?
    [y] Yes
    [n] No
    >> y
    Optional minimum value?
    >> abc
    Not a number
    Optional minimum value?
    >> 1500
    Optional maximum value?
    >> 9999

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> e

    Collections:
    (1) Teachers (0 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    ```


    Now let's add two *instances* for the `Teachers` collection. Again, notice the error handling.

    ```
    Collections:
    (1) Teachers (0 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> 1

    Teachers collection:
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> Frank van Viegen
    Start date [dd-mm-yyyy]?
    >> 01-01-2018
    Salary?
    >> 4000


    Teachers collection:
    (1) Frank van Viegen
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> 
    Name?
    >> Timothy Sealy
    Start date [dd-mm-yyyy]?
    >> 1-2-3
    Invalid date.
    Start date [dd-mm-yyyy]?
    >> 01-06-2015
    Salary?
    >> 500
    Value is lower than 1500.
    Salary?
    >> 4300
    ```

    At the end of the year, employees often receive a raise. Let's increase Frank's salary a bit:
    ```
    Teachers collection:
    (1) Frank van Viegen
    (2) Timothy Sealy
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> 1

    Teachers instance:
    (1) Name: Frank van Viegen
    (2) Start date: 01-01-2018
    (3) Salary: 4000
    [d] Delete instance
    [b] Go back
    >> 3
    Salary?
    >> 4100

    Teachers instance:
    (1) Name: Frank van Viegen
    (2) Start date: 01-01-2018
    (3) Salary: 4100
    [d] Delete instance
    [b] Go back
    >> b

    Collections:
    (1) Teachers (2 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    ```

    Now we'll create a `Modules` *collection*. One of its *attributes* links to the *instance* for the `Teacher` that is responsible for this module.
    ```
    Collections:
    (1) Teachers (2 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> a
    Collection name (eg. 'Employees' or 'Vehicles')?
    >> modules
    Let's add some attributes. The first attribute will be used when showing a collection list.

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 1
    Attribute label?
    >> name
    Is this a required field?
    [y] Yes
    [n] No
    >> y

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 2
    Attribute label?
    >> ects
    Is this a required field?
    [y] Yes
    [n] No
    >> y
    Optional minimum value?
    >> 1
    Optional maximum value?
    >> 60

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 4
    Attribute label?
    >> responsible teacher
    Is this a required field?
    [y] Yes
    [n] No
    >> y
    Which collection would you like to link to?
    (1) Teachers (2 instances)
    (2) Modules (0 instances)
    >> 1

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> e
    
    Collections:
    (1) Teachers (2 instances)
    (2) Modules (0 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    ```

    Let's add two *instances* to the `Modules` *collection*:
    ```
    Modules collection:
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> SQL
    Ects?
    >> 5
    Responsible teacher:
    (1) Frank van Viegen
    (2) Timothy Sealy
    >> 2


    Modules collection:
    (1) SQL
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> Static web
    Ects?
    >> 5
    Responsible teacher:
    (1) Frank van Viegen
    (2) Timothy Sealy
    >> 1


    Modules collection:
    (1) SQL
    (2) Static web
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    ```

    And for a final *collection*, let's create `Students`. It'll link to three other instances: a `study coach` (from the `Teachers` collection), a `current module` (from the `Modules` collection) and a `mentor` (from the `Students` collection itself).

    ```
    Collections:
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> a
    Collection name (eg. 'Employees' or 'Vehicles')?
    >> students
    Let's add some attributes. The first attribute will be used when showing a collection list.

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 1
    Attribute label?
    >> name
    Is this a required field?
    [y] Yes
    [n] No
    >> y

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 2
    Attribute label?
    >> student id
    Is this a required field?
    [y] Yes
    [n] No
    >> y
    Optional minimum value?
    >> 100000
    Optional maximum value?
    >> 

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 3
    Attribute label?
    >> start date
    Is this a required field?
    [y] Yes
    [n] No
    >> n

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 4
    Attribute label?
    >> study coach
    Is this a required field?
    [y] Yes
    [n] No
    >> y
    Which collection would you like to link to?
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (0 instances)
    >> 1

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 4
    Attribute label?
    >> current module
    Is this a required field?
    [y] Yes
    [n] No
    >> n
    Which collection would you like to link to?
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (0 instances)
    >> 2

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> 4
    Attribute label?
    >> mentor
    Is this a required field?
    [y] Yes
    [n] No
    >> n
    Which collection would you like to link to?
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (0 instances)
    >> 3

    Select an attribute type:
    (1) Text
    (2) Number (optionally bounded)
    (3) Date
    (4) Link (to another instance)
    [e] End of attributes
    >> e
    ```

    And next we'll add a couple of student *instances*:

    ```
    Collections:
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (0 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> 3

    Students collection:
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> Alice
    Student id?
    >> 123456
    Start date [dd-mm-yyyy]?
    >> 01-02-2021
    Study coach:
    (1) Frank van Viegen
    (2) Timothy Sealy
    >> 1
    Current module:
    (1) SQL
    (2) Static web
    [n] None
    >> 1
    Mentor:
    (1) Alice
    [n] None
    >> n


    Students collection:
    (1) Alice
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    >> a
    Name?
    >> Bob
    Student id?
    >> 234567
    Start date [dd-mm-yyyy]?
    >> 
    Study coach:
    (1) Frank van Viegen
    (2) Timothy Sealy
    >> 2
    Current module:
    (1) SQL
    (2) Static web
    [n] None
    >> 2
    Mentor:
    (1) Alice
    (2) Bob
    [n] None
    >> 1


    Students collection:
    (1) Alice
    (2) Bob
    [a] Add an instance
    [d] Delete collection
    [b] Go back
    ```

    And finally, we'll have the application generate a dependency graph and exit.

    ```
    Collections:
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (2 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> g
    graph.png written.

    Collections:
    (1) Teachers (2 instances)
    (2) Modules (2 instances)
    (3) Students (2 instances)
    [a] Add a collection
    [g] Graph links
    [x] Exit
    >> x
    ```

    This is what the generated graph looks like:

    <img src="graph.png" alt="Dependency graph">


- Upfront class diagram:
    -
        link: https://sd42.nl/curriculum/class-diagrams1#latest
        title: LMS42 - 1.2 OOP - Class diagrams
        info: Look back at this lesson if you want to refresh your memory on class diagrams (or in case you did OOP before class diagrams were introduced).

    - |
        The class diagrams you have seen in the OOP module were created using PlantUML, the same thing you used to create the ERDs for SQL. This has several advantages over using a GUI-based drawing tool:

        - It's a lot quicker to do (once you've learned how to do it).
        - The descriptions can be embedded into a Markdown (`.md`) document and live within your source tree.
        - It's easier to make changes and keep up to date with your source code. A search/replace on the source tree, for instance to rename a class, will modify the diagram as well.
        - The diagram can be easily easily managed with version control systems (*git*), just like the rest of the source code.

        In order to view PlantUML within Markdown from within VSCode, you need to install:
        - The `graphviz` and `jre-openjdk` Manjaro packages in *Add/Remove Software*.
        - The *Markdown Preview Enhanced* VSCode extension by *shd101wyy*.
        
        After installing, you can press `ctrl-k v` (hold ctrl, press k, release ctrl, press v) when you have an `.md` file open in order to render a nicely formatted document that includes class diagrams side-by-side with the source markdown.
    -
        link: https://www.youtube.com/watch?v=xObBUVDMbQs
        title: How to Make Easy UML Sequence Diagrams and Flow Charts with PlantUML
        info: This video gives a general introduction on PlantUML and does a good job at trying to get you enthused.
    -
        link: https://plantuml.com/class-diagram
        title: PlantUML - Class Diagram
        info: The official documentation, demoing all *class diagram* features. Note that PlantUML can also be used to create other types of diagrams, such as sequence diagrams, gantt diagrams, and many more. We'll only be using class diagrams for now though.
    -
        2: The diagram is obviously flawed, but still has value.
        3: The diagram gets us 80% of the way towards implementation. The other 20% requires some ugly hacks.
        4: The diagram gets us 90% of the way towards implementation. The other 10% is easy to improvise.
        text: |
            Design the application using a detailed class diagram in `DESIGN.md`, including the most important methods and attributes. Your diagram should use inheritance, composition and *abstract*.

            Here are some steps you can follow:
            - Create the classes. You can use noun-analysis for this, like with ERDs.
            - Create associations between the classes. These can be just plain lines at first, but should evolve to have details such as directions, associations and sometimes labels later.
            - Add the most important public methods.
            - Add the (private) attributes and perhaps some private methods.

            You will want to iterate through those steps a couple of times, constantly refining your design based on what you learned in the later steps.

    - |
        <div class="notification is-warning">You may want to have your design checked by a class mate or a teacher before you continue.</div>

- Implementation:
    -
        ^merge: feature
        text: |
            Implement the ERD as specified in the *Feature overview* and the *Example output*, based on your class diagram. It is normal to have to deviate from the plan a little bit, due to issues you didn't foresee when designing your class diagram.
            
            Hopefully there'll only be small issues, in which case you should leave your *Up-front design* as is, but just show the changes in your *As-built design*. If the issues you're encountering cannot be worked around without changing the whole design, you should go back to the previous objective and create a new *Up-front design*.

            ## Hints

            - Use `pickle` to save/load state.
            - Use the `pygraphviz` package to help you draw the graph.
            - There are a lot of menus that all work the same: the user is ask to select either a special option (`[a] Add a collection`, `[g] Graph links`) or a numbered item from a list. We recommend that you create a helper function for this, to prevent a lot of code duplication.

            ## Testing
            You can test if your implementation does *exactly* what is shown in the *Example output* by running the commands below in the terminal. The end result should be a correct processing of the lines in `input.txt` which will generate a graph shown in *Example output*.

            ```sh
            rm name-of-your-storage-file
            poetry run python your-program.py < input.txt
            ```

            What the command above does is feed a line from the `input.txt` file to your program whenever it asks for user input. To get a better understanding of how this works you can run the provided `test.py` program as follows:
            
            ```sh
            python test.py < input.txt
            ```

        4: Works as expected with `poetry run python your-program.py < input.txt`.

- As-built class diagram:

    -
        1: 50% correct (but lacking detail).
        2: 75% correct (but lacking detail).
        3: Gives a good overview, but lacks detail and/or has some minor errors.
        4: Perfect, in full detail.
        weight: 0.5
        text: |
            Make a copy of your *Upfront* design, and modify it to closely reflect your implementation.
