#!/usr/bin/env python

import sys

# Parse the command line arguments, to get the filename and to find out if we
# should run in debug mode (which should output a lot of info about what the
# virtual CPU is doing).
debug = False
debug_ops = []
debug_dumps = []
arg_pos = 1

if len(sys.argv) >= 2 and sys.argv[1] == "-d":
    debug = True
    arg_pos += 1
if len(sys.argv) != arg_pos+1:
    sys.exit(f"Syntax: {sys.argv[0]} [-d] <program_file>")
filename = sys.argv[arg_pos]

ADDRESS_SIZE = 2 # Memory addresses are expressed as two bytes (16 bits)
INSTRUCTION_POINTER_ADDRESS = 0 # The *instruction pointer* (the memory address where the next instruction should be read from) is located at address 0.
MEMORY_SIZE = 64*1024 # The computer has 64 kB memory

# The list of instructions, indexable by opcode.
INSTRUCTIONS = ["INVALID", "set", "add", "sub", "mul", "div", "and", "or", "xor", "if=", "if>", "if<", "read", "write"]

# An object to keep track of the current state of your VM
class VM:
    def __init__(self, memory):
        self.memory = memory
        self.current_address = self.load_data(INSTRUCTION_POINTER_ADDRESS, ADDRESS_SIZE)

    def start_instruction(self):
        global debug_dumps, debug_ops

        # clear debugging output
        debug_ops = []
        debug_dumps = []

        self.current_address = self.load_data(INSTRUCTION_POINTER_ADDRESS, ADDRESS_SIZE)
        return self.current_address != 0

    def save_instruction_pointer(self):
        self.store_data(INSTRUCTION_POINTER_ADDRESS, self.current_address, ADDRESS_SIZE)

    # What follows are a few utility functions that you may want to use in your implementation.
    def load_data(self, address, size):
        """Returns the integer starting at `address` consisting of `size` bytes."""
        return int.from_bytes(self.memory[address:address+size], byteorder='little', signed=False)

    def store_data(self, address, value, size):
        """Stores the integer `value` using `size` bytes starting at `address`."""
        self.memory[address:address+size] = value.to_bytes(length=size, byteorder='little')

    def read_console_byte(self):
        """Wait for a single byte to be typed by the user and returns it as a number."""
        ch = sys.stdin.read(1)
        return 256 if not ch else ord(ch)

    def write_console_byte(self, value):
        """Write a single byte (given as the number `value`) to the console."""
        sys.stdout.write(chr(value & 255))
        sys.stdout.flush()

    @staticmethod
    def from_filename(filename: str):
        # Create a bytearray that will serve as the virtual computer's memory, and load
        # the program into it.
        memory = bytearray(MEMORY_SIZE)
        with open(filename, 'rb') as file:
            program = file.read()
        memory[0:len(program)] = program

        return VM(memory)

# Here are some convenient classes to keep track of your instructions
class Argument:
    def __init__(self, address: int, value: int):
        self.address = address
        self.value = value

class Instruction:
    def __init__(self, address: int, opcode: int, integer_size: int, argument1: Argument, argument2: Argument):
        self.address = address
        self.opcode = opcode
        self.integer_size = integer_size
        self.argument1 = argument1
        self.argument2 = argument2

# TODO: Implement a function that uses the instruction_pointer to read the current instruction from memory.
#       If decoded correctly the instruction_pointer should afterwards point to the start of the *next* instruction.
#       This function should return the Instruction with all its information, as described by the class above.
def load_instruction(vm: VM) -> Instruction:
    pass

# TODO: Implement a function that executes an instruction.
def execute_instruction(vm: VM, instruction: Instruction):
    pass
# TODO: The actual CPU simulation loop!

# NOTE: You have to keep track of the instruction pointer value located at INSTRUCTION_POINTER_ADDRESS (0x0000)
#       You can make use of the utility functions and classes given above.
#       The first byte of the first instruction is then located at instruction_pointer and the second byte
#       at instruction_pointer + 1, etc.
#       If the instruction_pointer value should ever become zero the program should halt.


import unittest

class TestVM(unittest.TestCase):
    # a convenient helper method so you don't need to do this in every test
    @staticmethod
    def get_parsed_instruction(program):
        vm = VM(program)
        vm.start_instruction()
        return load_instruction(vm)

    def test_ip(self):
        vm = VM(b"\x42\x00")
        vm.start_instruction()
        self.assertEqual(vm.current_address, 0x42)

    def test_parse_subtraction(self):
        instruction = TestVM.get_parsed_instruction(b"\x02\x00\x03\x19\x02\x00\x01\x00")
        self.assertEqual(instruction.opcode, 3)
        self.assertEqual(instruction.integer_size, 2)
        self.assertEqual(instruction.argument1.address, 2)
        self.assertEqual(instruction.argument2.value, 1)

    # TODO add more unit tests