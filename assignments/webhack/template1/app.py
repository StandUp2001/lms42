from flask import Flask, request, redirect, make_response, g as request_context
from typing import Any
# README: Docs about the library postgresqlite: https://github.com/vanviegen/postgresqlite
import postgresqlite
from urllib import parse
import quoter_templates as templates

# Run using `poetry install && poetry run flask run --reload`.
app = Flask(__name__)

# Configure where static files are located and how long they're cached.
app.static_folder = '.'
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 1200
app.config['SECRET_KEY'] = "all your base are belong to us"
# Set up a database connection.
db: Any = postgresqlite.connect()

# Log all requests for analytics purposes
log_file = open('access.log', 'a', buffering=1)  # noqa: SIM115
@app.before_request
def log_request():
    log_file.write(f"{request.method} {request.path} {dict(request.form) if request.form else ''}\n")


# Set user_id on request if user is logged in, or else set it to None.
@app.before_request
def check_authentication():
    if 'user_id' in request.cookies:
        request_context.user_id = int(request.cookies['user_id'])
    else:
        request_context.user_id = None


# The main page
@app.route("/")
def index():
    quotes = db.query("select id, text, attribution from quotes order by id")
    return templates.main_page(quotes, request_context.user_id, request.args.get('error'))


# The quote comments page
@app.route("/quotes/<int:quote_id>")
def get_comments_page(quote_id):
    quote = db.query_row(f"select id, text, attribution from quotes where id={quote_id}")
    comments = db.query(f"select text, time, name as user_name from comments c left join users u on u.id=c.user_id where quote_id={quote_id} order by c.id")
    return templates.comments_page(quote, comments, request_context.user_id)


# Post a new quote
@app.route("/quotes", methods=["POST"])
def post_quote():
    db.query(f"""insert into quotes(text,attribution) values('{request.form["text"]}','{request.form["attribution"]}')""")
    return redirect("/#bottom")


# Post a new comment
@app.route("/quotes/<int:quote_id>/comments", methods=["POST"])
def post_comment(quote_id):
    db.query(f"""insert into comments(text,quote_id,user_id) values('{request.form["text"]}',{quote_id},{request_context.user_id})""")
    return redirect(f"/quotes/{quote_id}#bottom")


# Sign in user
@app.route("/signin", methods=["POST"])
def signin():
    username = request.form["username"].lower()
    password = request.form["password"]

    user = db.query_row(f"select id, password from users where name='{username}'")
    if user: # user exists
        if password != user['password']:
            # wrong! redirect to main page with an error message
            return redirect('/?error='+parse.quote("Invalid password!"))
        user_id = user['id']
    else: # new sign up
        user_id = db.query_value(f"insert into users(name,password) values('{username}', '{password}') returning id")

    response = make_response(redirect('/'))
    response.set_cookie('user_id', str(user_id))
    return response


# Sign out user
@app.route("/signout", methods=["GET"])
def signout():
    response = make_response(redirect('/'))
    response.delete_cookie('user_id')
    return response
