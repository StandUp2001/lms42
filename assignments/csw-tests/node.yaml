name: End-to-end tests
description: End-to-end testing using Cypress.
goals:
    e2etests: 1
days: 1
assignment:
    Assignment: |
        Let's create an end-to-end test for your Svelte ToDo app, using Cypress.
    Getting started: 
        - |
            - Copy your solution to the *Svelte* assignment, both frontend and backend.
            - Start the frontend and backend servers.
            - Within the frontend app, use npm to install `cypress`. Run it using `npx cypress open`, and it will generate a bunch of example test specs that you'll probably find useful.
            - Within `cypress/e2e` create a new `.cy.js` file, and start writing your tests. Something to get you started:

            ```js
            describe('ToDo', () => {
                it('loads the add-list dialog', () => {
                    // Open the page
                    cy.visit('http://127.0.0.1:5000');

                    // Click the 'add list' icon
                    cy.contains('note_add').click();

                    // Verify that the right text shows up
                    cy.contains('Create a new list');
                });
            });
            ```

        -
            link: https://www.youtube.com/watch?v=7N63cMKosIE
            title: Cypress End-to-End Testing
            info: A good introductory video. Although it talks about an app build using the Angular framework, the same applies for a Svelte web app.
        -
            link: https://docs.cypress.io/guides/getting-started/writing-your-first-test
            title: Cypress - Writing Your First Test 
            info: You'll want to at least gloss over everything on this page.
        -
            link: https://example.cypress.io/commands/querying
            title: Cypress - Querying
            info: Some useful examples and best practices for matching DOM elements from your tests.
        
        
    Resetting the database: |
        - Add a `/reset_database` route to the backend, that deletes all database tables and reapplies the `schema.sql`. You can look for 'inspiration' in the *REST & test* assignment backend. 
        - Use the Cypress `beforeEach` function and `cy.request` (like in *REST and test*) from within `cypress/support/e2e.js` to trigger the `/reset_database` route and reset your app to a known state before each test. Remember to use the right port number for the *backend* server in the URL. 

    Objective:
        -
            ^merge: feature
            text: |
                Create a series of tests that cover at least:
                
                - Creating new lists.
                - Creating new items.
                - Changing item priorities.
                - Changing item names.
                - Checking/unchecking an item.
                - Reloading the page after each of the above changes, to confirm that changes are persisted. 
                - Sorting of (newly created and renamed) items.
                - Showing/hiding priority headers depending on there being items of that priority.
