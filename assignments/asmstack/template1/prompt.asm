# The stack pointer, it always hold the next address where something
# can be pushed onto it. We initialize it to the bottom of the stack.
sp: int16 stackBottom

macro call FUNCTION {
	set16 **sp *ip # Copy the address of this instruction into **sp
	add16 **sp 24 # skip the 24 bytes for all instructions in this macro
	add16 *sp 2 # Increment the stack pointer
	set16 *ip FUNCTION
}

# General purpose 'registers'
a: int32 0
b: int32 0
c: int32 0

floorsQuestion:
"How many floors of stairs do you walk up on an average day? "
int8 0 # A zero-byte to indicate the end of the string

weightQuestion:
"How much do you weigh in kg? "
int8 0 # Zero-byte

moreAnswer:
"You spend more energy climbing stairs than Frank. Keep it up!\n"
int8 0 # Zero-byte

lessAnswer:
"You spend less energy climbing stairs than Frank. Challenge accepted? :-)\n"
int8 0 # Zero-byte



# ---- printString ----
# Arguments:
# - a: the 16-bit address of a zero-terminated string
# Destroys:
# - a

printString:
    # TODO!




# ---- prompt ----
# Arguments:
# - a: the 16-bit address of a zero-terminated string
# Returns:
# - a: the 32-bit integer read from the console
# Destroys:
# - b

prompt:
    # TODO!



# Our main logic. You shouldn't need to change anything here,
# but it's probably worth studying.
main:
    # First prompt
    set16 *a floorsQuestion
    call prompt

    # Store the answer to the first prompt in c
    set32 *c *a 

    # Second prompt
    set16 *a weightQuestion
    call prompt

mainAnswer:
    # The answers to the prompts are in c and a.
    mul32 *a *c # a becomes weight*floors

    # Set b to the address of the appropriate answer string
    set16 *b moreAnswer
    if<32 *a 1050 set16 *b lessAnswer

    # Print the answer string
    set16 *a *b
    call printString

    # End program
    set16 *ip 0


# The stack starts here. It has quite a bit of room to grow: the size of 
# the architecture's memory (65536 bytes) minus the size of the program.
stackBottom:
