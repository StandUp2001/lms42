﻿using Xunit;

namespace ClassDiagrams;

public class TestHue
{
    [Fact]
    public void End_To_End_Test()
    {
        //First test, Add a room
        //Arrange
        Hub hub = new Hub();
        Room kitchen = new Room("kitchen");

        //Act
        hub.AddRoom(kitchen);

        //Assert
        Assert.Equal("kitchen", kitchen.Name);
        Assert.Equal("kitchen {\n}", kitchen.ToString());
        Assert.Equal(kitchen, hub.GetRoom("kitchen"));
        Assert.Null(hub.GetRoom("no such room"));

        //Second test, Add a light
        //Arrange
        Light kitchenCeiling = new Light("ceiling");

        //Act
        kitchen.AddLight(kitchenCeiling);

        //Assert
        Assert.Equal("ceiling", kitchenCeiling.Name);
        Assert.Equal("ceiling → l0", kitchenCeiling.ToString());
        Assert.Equal(kitchenCeiling, kitchen.GetLight("ceiling"));
        Assert.Null(kitchen.GetLight("No Such Light"));
        Assert.Equal("kitchen {\n    ceiling → l0\n}", kitchen.ToString());

        //Third test Brightness
        //Act
        kitchenCeiling.SetState(new LightState(42));

        //Assert
        Assert.Equal("kitchen {\n    ceiling → l42\n}", kitchen.ToString());

        //Fourth test ColorLights
        //Arrange
        hub.AddRoom(new Room("bedroom"));
        Room bedroom = hub.GetRoom("bedroom");

        //Act
        bedroom.AddLight(new Light("bedside left"));
        bedroom.AddLight(new Light("bedside right"));
        bedroom.AddLight(new ColorLight("closet"));

        //Assert
        Assert.Equal("bedroom {\n    bedside left → l0\n    bedside right → l0\n    closet → l0 h0 s0\n}", bedroom.ToString());

        //Fifth test Set Atmosphere
        //Arrange
        LightState deepRed = new ColorLightState(30, 324, 100);
        bedroom.SetState(deepRed);

        //Assert
        Assert.Equal("bedroom {\n    bedside left → l30\n    bedside right → l30\n    closet → l30 h324 s100\n}", bedroom.ToString());

        //Act
        bedroom.GetLight("bedside right").SetState(new LightState(50));

        //Assert
        Assert.Equal("bedroom {\n    bedside left → l30\n    bedside right → l50\n    closet → l30 h324 s100\n}", bedroom.ToString());

        //Sixth test Scenes
        bedroom.StoreScene("romantic reading");
        bedroom.SetState(new LightState(100));

        Assert.Equal("bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}", bedroom.ToString());
        bedroom.StoreScene("bright");

        bedroom.ActivateScene("romantic reading");
        Assert.Equal("bedroom {\n    bedside left → l30\n    bedside right → l50\n    closet → l30 h324 s100\n}", bedroom.ToString());

        bedroom.ActivateScene("bright");
        Assert.Equal("bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}", bedroom.ToString());

        //Seventh test complete hub
        Assert.Equal("Hub>>>\nkitchen {\n    ceiling → l42\n}bedroom {\n    bedside left → l100\n    bedside right → l100\n    closet → l100\n}<<<Hub", hub.ToString());
    }
}