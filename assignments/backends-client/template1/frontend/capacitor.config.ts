import type { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'LikeNCraftBeer',
  webDir: 'dist'
};

export default config;
